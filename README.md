##Getting started

This library comes with a few presets that can be used to get started quickly.

For example, to test that a string contains a Symbol such as `#$%`, we can use the following preset
```php
$validate = Strings::ContainsSymbol();
var_dump($validate("sdflkj345"));
```    
Here the result would be false, as the above string doesn't contain a symbol. 

In addition to the presets already defined, you can additionally chain multiple validators
(presets or custom rules) together. 
Here is how the SecurePassword preset validator is created.
```php
return self::LengthRange(10, 100)
    ->andWith(self::ContainsSymbol())
    ->andWith(self::ContainsNumber())
    ->andWith(self::ContainsUppercase());
```
the above example chains 4 validators together to allow us to specify the requirements for a 
valid complex password. All validators (called Rules) have 2 methods available: `andWith` and `orWith`

All of the presets are made simply calling existing Rules. Currently there are a few basic rules that allow 
simple string verification tasks. 

Here for example is the `ContainsSymbol` preset definition
```php
static public function ContainsSymbol(int $count=1)
{
    return new StringContainsValidationRule('/[\W]/', $count);
}
```

##Going Deeper
Creating new Rules is as straight forward as extending `AbstractRule`.

```php
class LowerCaseStringValidationRule extends AbstractRule
{
    protected function validate($input): bool
    {
        $this->clearErrors();
        if($input === strtolower($input)) {
            return true;
        }
        $this->addError("{$input} is not lowercase");
        return false;
    }
}
```
Here is a basic example of a validation Rule that checks the the `$input` string is all in lowercase.

However you can write Rules that accept any type of input, and perform any amount of processing on it.