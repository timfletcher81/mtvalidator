<?php

namespace timfletcher\Validator\Rules;

use timfletcher\Validator\AbstractRule;

class PHPFilterValidationRule extends AbstractRule
{
    private $filter;
    public function __construct($filter)
    {
        $this->filter = $filter;
    }

    protected function validate($input): bool
    {
        $this->clearErrors();
        if(filter_var($input, $this->filter))
        {
            return true;
        };
        $this->addError("{$input} is not valid for this filter");
        return false;
    }
}