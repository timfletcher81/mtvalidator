<?php

namespace timfletcher\Validator\Rules;

use timfletcher\Validator\AbstractRule;

class LowerCaseStringValidationRule extends AbstractRule
{
    protected function validate($input): bool
    {
        $this->clearErrors();
        if($input === strtolower($input)) {
            return true;
        }
        $this->addError("{$input} is not lowercase");
        return false;
    }
}