<?php
namespace timfletcher\Validator\Rules;

use timfletcher\Validator\AbstractRule;

class StringLengthValidationRule extends AbstractRule
{
    private $min;
    private $max;
    public function __construct(int $min, int $max)
    {
        $this->min = $min;
        $this->max = $max;
    }

    protected function validate($input): bool
    {
        $this->clearErrors();
        $strlen = strlen($input);
        if(is_string($input) && ($strlen >= $this->min && $strlen <= $this->max))
        {
            return true;
        }
        $this->addError("The input string must be between {$this->min} and {$this->max}");
        return false;
    }
}