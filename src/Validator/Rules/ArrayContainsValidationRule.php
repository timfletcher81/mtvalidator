<?php

namespace timfletcher\Validator\Rules;

use timfletcher\Validator\AbstractRule;

class ArrayContainsValidationRule extends AbstractRule
{
    private $sourceArray;

    public function __construct(array $sourceArray)
    {
        $this->sourceArray = $sourceArray;
    }

    protected function validate($input): bool
    {
        $this->clearErrors();
        if(in_array($input, $this->sourceArray)) {
            return true;
        }
        $this->addError("source array does not contain {$input}");
        return false;
    }
}