<?php

namespace timfletcher\Validator\Rules;

use timfletcher\Validator\AbstractRule;

class NumberValidationRule extends AbstractRule
{
    protected function validate($input): bool
    {
        $this->clearErrors();
        if(is_numeric($input))
        {
            return true;
        }
        $this->addError("{$input} is not a valid number");
        return false;
    }
}