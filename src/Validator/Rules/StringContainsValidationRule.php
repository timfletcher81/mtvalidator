<?php
namespace timfletcher\Validator\Rules;

use timfletcher\Validator\AbstractRule;

class StringContainsValidationRule extends AbstractRule
{
    private $regex;
    private $count;
    public function __construct($regex, $count)
    {
        $this->regex = $regex;
        $this->count = $count;
    }

    protected function validate($input): bool
    {
        $this->clearErrors();
        if(preg_match_all($this->regex, $input) >= $this->count)
        {
            return true;
        }
        $this->addError("{$this->regex} was not found in input {$this->count} times");
        return false;
    }
}