<?php

namespace timfletcher\Validator;

/**
 * Class AbstractRule
 * @package timfletcher\Validator
 *
 */
abstract class AbstractRule {
    private $errors = [];
    abstract protected function validate($input): bool;

    public function getErrors(): array
    {
        return $this->errors;
    }

    protected function clearErrors(): void
    {
        $this->errors = [];
    }

    protected function addError(string $err)
    {
        $this->errors[] = $err;
    }

    protected function addErrors(array $errs)
    {
        $this->errors += $errs;
    }

    public function __invoke($input): bool
    {
        return $this->validate($input);
    }

    /**
     * @param AbstractRule $other the other AbstractRule that we want to chain
     * @return AbstractRule
     *
     * Used to chain the $a and $b rules together, and make sure that they both succeed.
     */
    public function andWith(AbstractRule $other): AbstractRule
    {
        return new class($this, $other) extends BinaryOperatorAbstractRule
        {
            protected function validate($input): bool
            {
                if($this->left->validate($input)) {
                    if ($this->right->validate($input))
                    {
                        return true;
                    }
                }
                $this->addErrors($this->right->getErrors());
                $this->addErrors($this->left->getErrors());
                return false;
            }
        };
    }

    /**
     * @param AbstractRule $other the other AbstractRule that we want to chain
     * @return AbstractRule
     *
     * Behaves the same as andWith method, except that either $a or $b need to be true.
     */
    public function orWith(AbstractRule $other): AbstractRule
    {
        return new class($this, $other) extends BinaryOperatorAbstractRule
        {
            protected function validate($input): bool
            {
                if($this->left->validate($input)) {
                    return true;
                }
                if ($this->right->validate($input))
                {
                    return true;
                }
                $this->addErrors($this->left->getErrors());
                $this->addErrors($this->right->getErrors());
                return false;
            }
        };
    }
}
