<?php

namespace timfletcher\Validator\Presets;

use timfletcher\Validator\AbstractRule;
use timfletcher\Validator\Rules\ArrayContainsValidationRule;

class CountryCodes
{
    private static function getJsonData($filePath)
    {
        $fullPath = join('/', [dirname(__FILE__), '../data', $filePath]);
        return json_decode(file_get_contents(realpath($fullPath)), true);
    }
    public static function Alpha2(): AbstractRule
    {
        $jsonData = self::getJsonData('iso-3166-alpha2.json');
        return new ArrayContainsValidationRule($jsonData);
    }

    public static function Alpha3(): AbstractRule
    {
        $jsonData = self::getJsonData('iso-3166-alpha3.json');
        return new ArrayContainsValidationRule($jsonData);
    }

    public static function Numeric(): AbstractRule
    {
        $jsonData = self::getJsonData('iso-3166-numeric.json');
        return new ArrayContainsValidationRule($jsonData);
    }

    public static function All(): AbstractRule
    {
        return self::Alpha2()
            ->orWith(self::Alpha3())
            ->orWith(self::Numeric());
    }
}