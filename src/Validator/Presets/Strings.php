<?php
namespace timfletcher\Validator\Presets;

use timfletcher\Validator\Rules\StringLengthValidationRule;
use timfletcher\Validator\Rules\StringContainsValidationRule;
use timfletcher\Validator\Rules\PHPFilterValidationRule;

class Strings
{
    public static function ContainsSymbol(int $count=1)
    {
        return new StringContainsValidationRule('/[\W]/', $count);
    }

    public static function ContainsUppercase(int $count=1)
    {
        return new StringContainsValidationRule('/[A-Z]/', $count);
    }

    public static function ContainsNumber(int $count=1)
    {
        return new StringContainsValidationRule('/[0-9]/', $count);
    }

    public static function LengthRange(int $min=10, int $max=100)
    {
        return new StringLengthValidationRule($min, $max);
    }

    public static function ValidEmail()
    {
        return new PHPFilterValidationRule(FILTER_VALIDATE_EMAIL);
    }

    public static function ValidURL()
    {
        return new PHPFilterValidationRule(FILTER_VALIDATE_URL);
    }

    public static function ValidDomain()
    {
        return new PHPFilterValidationRule(FILTER_VALIDATE_DOMAIN);
    }

    public static function SecurePassword()
    {
        return self::LengthRange()
            ->andWith(self::ContainsSymbol())
            ->andWith(self::ContainsNumber())
            ->andWith(self::ContainsUppercase());
    }
}