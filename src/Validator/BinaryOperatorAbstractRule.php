<?php

namespace timfletcher\Validator;

/**
 * Class BinaryOperatorAbstractRule
 * @package timfletcher\Validator
 *
 * This class is used to create a tree of AbstractRule leaves. Any AbstractRule can be chained
 * by using the andWith/orWith methods to other AbstractRule.
 */
abstract class BinaryOperatorAbstractRule extends AbstractRule
{
    protected $left = null;
    protected $right = null;

    public function __construct(AbstractRule $left, AbstractRule $right)
    {
        $this->left = $left;
        $this->right = $right;
    }

}