<?php

use timfletcher\Validator\Rules\EmailValidationRule;
use timfletcher\Validator\Rules\LowerCaseStringValidationRule;
use timfletcher\Validator\Rules\NumberValidationRule;
use timfletcher\Validator\Rules\StringLengthValidationRule;
use timfletcher\Validator\Rules\StringContainsValidationRule;
use timfletcher\Validator\Rules\ArrayContainsValidationRule;
use PHPUnit\Framework\TestCase;

class RulesTest extends TestCase
{

    public function testNumberValidation()
    {
        $validate = new NumberValidationRule();
        $this->assertFalse($validate("10+2"));
        $this->assertCount(1, $validate->getErrors());
        $this->assertTrue($validate("10"));
        $this->assertTrue($validate(0x10));
        $this->assertTrue($validate(0));
        $this->assertFalse($validate(true));
    }

    public function testStringLengthValidation()
    {
        $validate = new StringLengthValidationRule(3, 6);
        $this->assertFalse($validate("ab"));
        $this->assertCount(1, $validate->getErrors());
        $this->assertTrue($validate("abc"));
        $this->assertTrue($validate("abcd"));
        $this->assertTrue($validate("abcdef"));
        $this->assertFalse($validate("abcdefg"));
        $this->assertFalse($validate(1000));
        $this->assertCount(1, $validate->getErrors());

    }

    public function testStringContainsValidation()
    {
        $validate = new StringContainsValidationRule('/[\W]/', 2);
        $this->assertFalse($validate('asdf*sdf'));
        $this->assertTrue($validate('as*df$sdf'));
    }

    public function testArrayContainsValidation()
    {
        $validate = new ArrayContainsValidationRule([
            'AAA', 'BBB', 'CCC'
        ]);
        $this->assertTrue($validate('AAA'));
        $this->assertFalse($validate('AaA'));
        $this->assertTrue($validate('CCC'));
        $this->assertFalse($validate('x'));
    }

    public function testJoinRulesWithAnd()
    {
        $validate = (new StringLengthValidationRule(1, 10))->andWith(new LowerCaseStringValidationRule());
        $this->assertFalse($validate("invAlid-email-address@com"));
        $this->assertCount(1, $validate->getErrors());
        $this->assertTrue($validate("a@a.com"));
    }

    public function testJoinRulesWithOr()
    {
        $validate = (new StringLengthValidationRule(1,3))
            ->orWith(new StringLengthValidationRule(5, 7))
            ->orWith(new StringLengthValidationRule(10, 12));

        $this->assertTrue($validate(str_repeat(' ', 2)));
        $this->assertFalse($validate(str_repeat('h', 4)));
        $this->assertFalse($validate(str_repeat('$', 0)));
        $this->assertTrue($validate(str_repeat(' ', 5)));
        $this->assertTrue($validate(str_repeat('A', 7)));
        $this->assertFalse($validate(str_repeat(' ', 8)));
        $this->assertTrue($validate(str_repeat('1', 10)));
        $this->assertFalse($validate(str_repeat(' ', 13)));
    }
}