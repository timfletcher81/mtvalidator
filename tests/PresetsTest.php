<?php

use timfletcher\Validator\Presets\CountryCodes;
use timfletcher\Validator\Presets\Strings;
use PHPUnit\Framework\TestCase;

class PresetsTest extends TestCase
{
    public function testCountryCodesAlpha2()
    {
        $validate = CountryCodes::Alpha2();
        $this->assertTrue($validate('GB'));
        $this->assertFalse($validate('UK'));
    }

    public function testCountryCodesAlpha3()
    {
        $validate = CountryCodes::Alpha3();
        $this->assertTrue($validate('GBR'));
        $this->assertTrue($validate('UKR'));
        $this->assertFalse($validate('GB'));
        $this->assertFalse($validate('UK'));
    }

    public function testCountryCodesNumeric()
    {
        $validate = CountryCodes::Numeric();
        $this->assertTrue($validate('004'));
        $this->assertTrue($validate('533'));
        $this->assertFalse($validate('000'));
        $this->assertFalse($validate('999'));
    }

    public function testCountryCodesAll()
    {
        $validate = CountryCodes::All();
        $this->assertTrue($validate('004'));
        $this->assertTrue($validate('533'));
        $this->assertFalse($validate('000'));
        $this->assertFalse($validate('999'));
        $this->assertTrue($validate('GB'));
        $this->assertFalse($validate('UK'));
        $this->assertTrue($validate('GBR'));
        $this->assertTrue($validate('UKR'));
        $this->assertFalse($validate('AAAAA'));
        $this->assertFalse($validate('ZXC'));

    }

    public function testSecurePassword()
    {
        $validate = Strings::SecurePassword();
        $this->assertFalse($validate('asdasd0^'));
        $this->assertFalse($validate('asdasd0^aa'));
        $this->assertFalse($validate('asdasd00aa'));
        $this->assertTrue($validate('aSdasd0^aa'));
    }

    public function testContainsSymbol()
    {
        $validate = Strings::ContainsSymbol(1);
        $this->assertFalse($validate('asdfjkbbrfvr'));
        $this->assertFalse($validate('33434645764567'));
        $this->assertTrue($validate('@'));
        $validate = Strings::ContainsSymbol(2);
        $this->assertFalse($validate('33434645764567'));
        $this->assertFalse($validate('@'));
        $this->assertTrue($validate('@ '));
    }

    public function testContainsUppercase()
    {
        $validate = Strings::ContainsUppercase(1);
        $this->assertFalse($validate('asdfjkbbrfvr'));
        $this->assertFalse($validate('334346457 64567'));
        $this->assertTrue($validate('asdfjkbbrfvr@1123 B'));
    }

    public function testContainsNumber()
    {
        $validate = Strings::ContainsNumber(1);
        $this->assertFalse($validate('asdfjkbbrfvr'));
        $this->assertTrue($validate('334346457 64567'));
        $this->assertTrue($validate('asdfjkbbrfvr@1123 B'));
    }

    public function testLengthRange()
    {
        $validate = Strings::LengthRange(1,2);
        $this->assertFalse($validate(''));
        $this->assertFalse($validate('abc'));
        $this->assertTrue($validate('a'));
        $this->assertTrue($validate('as'));
    }
    public function testValidEmailAddress()
    {
        $validate = Strings::ValidEmail();
        $this->assertTrue($validate("email-address@example.com"));
        $this->assertFalse($validate("invalid-email-address@com"));
    }

    public function testEmailAddressError()
    {
        $validate = Strings::ValidURL();
        $this->assertFalse($validate("http//google.com"));
        $this->assertFalse($validate("http//google."));
        $this->assertTrue($validate("ftp://email-address.example.com"));
        $this->assertTrue($validate("xyz://abc:efg@email-address.example.com"));
    }

}
